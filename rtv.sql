CREATE DATABASE  IF NOT EXISTS `rtv` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rtv`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: rtv-database.chkiapksq8pz.me-south-1.rds.amazonaws.com    Database: rtv
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `rtv3_account`
--

DROP TABLE IF EXISTS `rtv3_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Password` text NOT NULL,
  `Joins` int(11) NOT NULL DEFAULT '0',
  `OriginIP` text NOT NULL,
  `IP` text NOT NULL,
  `UID` text NOT NULL,
  `UID2` text NOT NULL,
  `AdminLevel` int(11) NOT NULL DEFAULT '0',
  `Ban` text,
  `Mute` text,
  `RPGBan` text,
  `DateReg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLog` int(11) NOT NULL,
  `ReadNews` int(11) NOT NULL DEFAULT '0',
  `Sounds` int(11) NOT NULL DEFAULT '1',
  `Jingles` int(11) NOT NULL DEFAULT '1',
  `AutoRespawn` int(11) NOT NULL DEFAULT '0',
  `SpawnBan` int(11) NOT NULL DEFAULT '0',
  `RobberCoins` int(11) NOT NULL DEFAULT '0',
  `IsActivated` int(11) NOT NULL DEFAULT '1',
  `Comments` text,
  `Stealth` int(11) NOT NULL DEFAULT '0',
  `Mapper` int(11) NOT NULL DEFAULT '0',
  `ChatMode` text NOT NULL,
  `MVPSound` int(11) NOT NULL DEFAULT '0',
  `NoMVP` text NOT NULL,
  `IsOnline` text NOT NULL,
  `TeamESP` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=898 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_account`
--

LOCK TABLES `rtv3_account` WRITE;
/*!40000 ALTER TABLE `rtv3_account` DISABLE KEYS */;
INSERT INTO `rtv3_account` VALUES (893,'kini','064f2768b9dba1b3c3e9929f65f597b6ecf69aa1c28f7e06379099da5a707e69',0,'182.77.47.60','182.77.47.60','62a71247a0fac48b27da2fcf9223c80c56a0b854','79ec8f3edc9b62ff387741d3697fd1d837742cf7',0,NULL,NULL,NULL,'0000-00-00 00:00:00',1586817720,0,1,1,0,0,0,1,NULL,0,0,'',0,'','',NULL),(894,'[MK]SahiL','6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090',3,'47.30.188.33','0.0.0.0','45dc56d324df443267f100463192902293e1f7fa','618cb92c423bab4a6b454c4c8538cd0a389bc7ea',1,'N/A','N/A','N/A','0000-00-00 00:00:00',1586847605,0,1,1,0,0,0,1,NULL,0,0,'',0,'false','0','false'),(895,'[VU]aXXo','1b4f0e9851971998e732078544c96b36c3d01cedf7caa332359d6f1d83567014',0,'182.77.47.60','182.77.47.60','62a71247a0fac48b27da2fcf9223c80c56a0b854','79ec8f3edc9b62ff387741d3697fd1d837742cf7',0,NULL,NULL,NULL,'0000-00-00 00:00:00',1586844265,0,1,1,0,0,0,1,NULL,0,0,'',0,'','',NULL),(896,'D4V3','1b4f0e9851971998e732078544c96b36c3d01cedf7caa332359d6f1d83567014',0,'182.77.47.60','182.77.47.60','62a71247a0fac48b27da2fcf9223c80c56a0b854','79ec8f3edc9b62ff387741d3697fd1d837742cf7',0,NULL,NULL,NULL,'0000-00-00 00:00:00',1586844376,0,1,1,0,0,0,1,NULL,0,0,'',0,'','',NULL),(897,'[VU]BliTz','9cafe03be4fb99a640097a3b5e0db6a77c5bda1527db9556ef325b16e40127c4',0,'183.82.219.136','183.82.219.136','26c4b8518435f418e3ca003d55b0f1ec469f8c58','8e51624d1ea29b24645dba607da0ae6eb4e9a108',0,NULL,NULL,NULL,'0000-00-00 00:00:00',1586846106,0,1,1,0,0,0,1,NULL,0,0,'',0,'','',NULL);
/*!40000 ALTER TABLE `rtv3_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_bases`
--

DROP TABLE IF EXISTS `rtv3_bases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_bases` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `DefPos` text NOT NULL,
  `AttPos` text NOT NULL,
  `CheckpointPos` text NOT NULL,
  `VehicleModel` int(11) NOT NULL,
  `VehiclePos` text NOT NULL,
  `VehicleAngle` varchar(255) NOT NULL,
  `TopPlayer` int(11) DEFAULT '0',
  `Score` int(11) NOT NULL DEFAULT '0',
  `Author` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_bases`
--

LOCK TABLES `rtv3_bases` WRITE;
/*!40000 ALTER TABLE `rtv3_bases` DISABLE KEYS */;
INSERT INTO `rtv3_bases` VALUES (1,'Pueblo de los Banghos','-1196.87, 372.487, 8.02279','-1261.31, 194.207, 10.7683','-1204.95, 95.7925, 11.1182',154,'-1154.63, 294.576, 11.263','2.3824',0,0,131),(2,'Haitian Drug Factory','-1162.85,106.154,11','-1026.74,82.0504,11.7','-1026.56,70.1524,11.434',187,'-1121.81, 84.4702, 11.1264','3.12103',0,0,0),(3,'Behind Cherry Popper','-938.888, -610.984, 12.4282','-971.715, -598.768, 11.4974','-951.493, -528.543, 11.0235',150,'-923.016, -524.873, 11.1572','0.068934',0,0,0),(4,'Construction Yards','331.163, -241.912, 29.6466','274.079, -129.656, 18.3909','272.481, -134.142, 11.8374',211,'257.045, -296.172, 10.0935','-0.19338',0,0,0),(5,'Sunshine Autos','-967.501, -829.178, 6.80088','-938.248, -957.829, 12.7327','-909.615, -1037.27, 14.7913',147,'-1017.75, -864.255, 17.9627','-2.45527',0,0,0),(6,'Fullmoon Mall','198.673, -937.034, 10.4318','119.475,-997.951,10.9','133.414,-997.562,10.6959',141,'186.056, -875.088, 12.226','1.16345',29,265,0),(7,'Cuban Garage','-1133.24, -476.346, 10.8309','-1081.35, -599.795, 11.3194','-1100,-625.435,11.3107',164,'-1046.53, -453.882, 11.0357','-2.94366',35,149,0),(8,'Rock City','-872.86, 692.13, 11.0846','-824.129, 986.45, 11.0454','-741.023, 1025.43, 11.0846',188,'-877.917, 794.262, 10.9149','-0.0211883',49,168,0),(9,'Spand Express','355.631, -318.111, 11.8069','164.43, -366.873, 11.6478','153.976, -365.391, 8.67068',213,'290.533, -305.551, 11.9591','-1.60006',21,218,0),(10,'Beach Wars','-215.685, -1639.48, 15.5355','-439.164, -1702.68, 26.579','-364.704, -1731.63, 8.04',154,'-306.306, -1657.34, 13.1576','-3.13081',10,130,0),(11,'Vicepoint Loadingbay','463.3, 287.938, 12.3319','331.742, 363.284, 11.4745','307.873, 376.468, 13.217',151,'458.413, 332.047, 11.8341','0.0589633',56,149,0),(12,'Elven City','606.106, -1056.27, 50.1893','559.464, -1149.32, 11.9895','557.251, -1184.52, 12.0736',154,'558.998, -1010.89, 11.1413','3.06791',7,149,0),(13,'Construction Wars #2','-230.775, -473.39, 177.413','-366.85, -490.72, 170.301','-377.256, -468.429, 170.628',230,'-224.695, -488.404, 170.645','-0.147169',68,383,0),(14,'Ruso bridge no existe','-531.278, -775.118, 13.4846','-397.98, -932.125, 23.7604','-375.133, -933.584, 22.2877',154,'-410.034, -770.291, 14.6741','3.01345',23,350,0),(15,'Las Barrancas','-773.491, 1435.7, 563.829','-761.072, 1635.81, 577.305','-752.224, 1634.87, 577.203',220,'-815.706, 1458.79, 564.296','1.51723',14,425,0),(16,'No Hay Escape','585.542, -1261.33, 12.8361','428.393, -1328.38, 23.5633','418.874, -1355.11, 11.0712',154,'575.637, -1414.4, 13.3061','0.801229',57,350,0),(17,'Custom Base - Temple','607.038, -1142.48, 775.21','397.806, -1134.5, 772.004','442.764, -1105.25, 775.176',200,'556.68, -1131.81, 773.75','1.23631',0,0,0),(18,'Fatty Bikers','-574.469, 643.767, 11.0712','-692.701, 747.789, 10.9149','-692, 704.359, 12.1164',188,'-563.473, 706.177, 20.5149','1.54024',1,350,0),(19,'Leaf Links','251.022, 501.271, 9.5805','58.1338, 255.546, 19.432','168.496, 241.373, 11.9211',175,'148.53, 447.664, 12.8465','-1.61866',26,216,162),(20,'Ship Wars','642.425, -1834.43, 15.5901','397.898, -1935.9, 15.5914','391.19, -1742.58, 15.4609',188,'632.27, -1798.06, 15.59','1.77417',26,880,134),(21,'G-Spot Downtown','-350.759, 939.142, 10.8527','-636.649, 872.532, 11.5987','-640.336, 849.243, 22.3633',198,'-358.33, 991.341, 47.3406','1.05051',0,0,0),(22,'Abandoned Terminal','-1807.68, -475.918, 14.363','-1345.99, -669.487, 14.3635','-1446.91, -613.648, 14.3635',226,'-1677.07, -445.4, 14.8678','1.31876',0,0,0),(23,'Vicepoint Helipad','529.979, 219.674, 14.493','294.022, 271.091, 17.71','298.621, 299.905, 16.3396',135,'549.331, 193.647, 16.0984','0.440355',2,616,0),(24,'Washington Brawl','128.993, -1104.71, 10.4466','-121.803, -926.887, 10.4634','-180.166, -977.574, 10.4633',175,'38.6659, -1088.32, 10.4633','1.55366',18,616,0),(25,'Pole Position Club','176.459, -1476.29, 10.9836','67.2431, -1445.55, 10.5655','64.1276, -1449.51, 10.5655',198,'190.346, -1453.76, 11.0712','-0.30287',12,616,0),(26,'WB Stunt Area','314.302, -324.187, 11.9592','527.259, -326.934, 13.8289','286.928, -301.187, 11.9591',191,'507.969, -307.016, 13.8289','-3.10799',12,702,0),(27,'Viceport Anarchy','-1037.17, -1451.56, 11.7671','-710.28, -1522.17, 11.8508','-672.836, -1479.34, 13.5666',175,'-962.862, -1393.1, 11.7809','-0.39606',16,616,0),(28,'Roxor International','-589.134, 1242.21, 11.0712','-421.266, 1266.2, 11.767','-448.337, 1299.84, 10.9043',150,'-518.033, 1221.33, 8.81041','2.58988',6,205,0),(29,'Junkyard V2','-1281.62, 204.059, 10.7964','-1163.01, 23.0408, 11.4965','-1161.97, -13.3214, 16.3182',198,'-1314.62, 165.641, 11.5229','-3.03953',1,616,0),(30,'Havanna Colony','-953.868, 30.5847, 10.5416','-947.402, -263.423, 10.7803','-994.138, -300.985, 10.7594',164,'-1002, -6.32925, 12.3619','-1.69632',16,616,0);
/*!40000 ALTER TABLE `rtv3_bases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_doodle`
--

DROP TABLE IF EXISTS `rtv3_doodle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_doodle` (
  `Text` tinytext,
  `Author` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_doodle`
--

LOCK TABLES `rtv3_doodle` WRITE;
/*!40000 ALTER TABLE `rtv3_doodle` DISABLE KEYS */;
/*!40000 ALTER TABLE `rtv3_doodle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_msg`
--

DROP TABLE IF EXISTS `rtv3_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_msg` (
  `type` text NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_msg`
--

LOCK TABLES `rtv3_msg` WRITE;
/*!40000 ALTER TABLE `rtv3_msg` DISABLE KEYS */;
INSERT INTO `rtv3_msg` VALUES ('','test'),('from_ingame','dont_send'),('from_discord','blala  '),('from_vl_ingame','yes okay ');
/*!40000 ALTER TABLE `rtv3_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_panel_stuff`
--

DROP TABLE IF EXISTS `rtv3_panel_stuff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_panel_stuff` (
  `rtv3_mysql_timeout` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_panel_stuff`
--

LOCK TABLES `rtv3_panel_stuff` WRITE;
/*!40000 ALTER TABLE `rtv3_panel_stuff` DISABLE KEYS */;
INSERT INTO `rtv3_panel_stuff` VALUES ('Keep this shit alive');
/*!40000 ALTER TABLE `rtv3_panel_stuff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_pstats`
--

DROP TABLE IF EXISTS `rtv3_pstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_pstats` (
  `ID` int(11) NOT NULL,
  `Kills` int(11) NOT NULL DEFAULT '0',
  `Deaths` int(11) NOT NULL DEFAULT '0',
  `Assist` int(11) NOT NULL,
  `Stolen` int(11) NOT NULL DEFAULT '0',
  `AttWon` int(11) NOT NULL DEFAULT '0',
  `DefWon` int(11) NOT NULL DEFAULT '0',
  `MVP` int(11) NOT NULL DEFAULT '0',
  `TopSpree` int(11) NOT NULL DEFAULT '0',
  `WeaponInfo` text,
  `Inventory` text,
  `XP` int(11) NOT NULL DEFAULT '0',
  `Level` int(11) NOT NULL DEFAULT '0',
  `Playtime` int(11) NOT NULL,
  `ReadNews` text NOT NULL,
  `RoundPlayed` int(11) NOT NULL,
  `Missions` text,
  `OperationScore` int(11) NOT NULL DEFAULT '0',
  `DailyReward` int(11) DEFAULT '0',
  `Carcol1` int(11) DEFAULT NULL,
  `Carcol2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_pstats`
--

LOCK TABLES `rtv3_pstats` WRITE;
/*!40000 ALTER TABLE `rtv3_pstats` DISABLE KEYS */;
INSERT INTO `rtv3_pstats` VALUES (893,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,'',0,NULL,0,0,NULL,NULL),(894,0,0,0,0,0,0,0,0,NULL,NULL,0,0,15,'true',0,'{\"Mission15\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission14\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission13\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission12\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission19\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission18\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission17\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission16\": {\"DateComplete\": 0,\"Progress\": 1},\"Mission31\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission30\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission20\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission11\": {\"DateComplete\": 0,\"Progress\": 15},\"Mission10\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission29\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission28\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission27\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission26\": {\"DateComplete\": 0,\"Progress\": 15},\"Mission25\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission24\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission23\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission22\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission9\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission21\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission7\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission8\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission5\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission6\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission3\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission4\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission1\": {\"DateComplete\": 0,\"Progress\": 0},\"Mission2\": {\"DateComplete\": 0,\"Progress\": 0}}',50,1586847605,0,0),(895,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,'',0,NULL,0,0,NULL,NULL),(896,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,'',0,NULL,0,0,NULL,NULL),(897,0,0,0,0,0,0,0,0,NULL,NULL,0,0,0,'',0,NULL,0,0,NULL,NULL);
/*!40000 ALTER TABLE `rtv3_pstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_server`
--

DROP TABLE IF EXISTS `rtv3_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_server` (
  `ServPass` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_server`
--

LOCK TABLES `rtv3_server` WRITE;
/*!40000 ALTER TABLE `rtv3_server` DISABLE KEYS */;
/*!40000 ALTER TABLE `rtv3_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_uid1`
--

DROP TABLE IF EXISTS `rtv3_uid1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_uid1` (
  `UID` text NOT NULL,
  `Ban` text,
  `Mute` text,
  `RPGBan` text,
  `Alias` text,
  `Comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_uid1`
--

LOCK TABLES `rtv3_uid1` WRITE;
/*!40000 ALTER TABLE `rtv3_uid1` DISABLE KEYS */;
INSERT INTO `rtv3_uid1` VALUES ('62a71247a0fac48b27da2fcf9223c80c56a0b854','N/A','N/A','N/A','{\"kini\": {\"LastUsed\":\"1586817720\",\"UsedTimes\": 2},\"D4V3\": {\"LastUsed\":\"1586844376\",\"UsedTimes\":\"1\"},\"[VU]aXXo\": {\"UsedTimes\":\"1\",\"LastUsed\":\"1586844265\"}}','N/A'),('45dc56d324df443267f100463192902293e1f7fa','N/A','N/A','N/A','{\"[MK]SahiL\": {\"UsedTimes\": 5,\"LastUsed\":\"1586847605\"}}','N/A'),('703b5354d7f36a2343ea5d87ac3ba31f7fae4246','N/A','N/A','N/A','N/A','N/A'),('26c4b8518435f418e3ca003d55b0f1ec469f8c58','N/A','N/A','N/A','{\"[VU]BliTz\": {\"LastUsed\":\"1586846106\",\"UsedTimes\": 2}}','N/A');
/*!40000 ALTER TABLE `rtv3_uid1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtv3_uid2`
--

DROP TABLE IF EXISTS `rtv3_uid2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rtv3_uid2` (
  `UID` text NOT NULL,
  `Ban` text,
  `Mute` text,
  `RPGBan` text,
  `Alias` text,
  `Comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtv3_uid2`
--

LOCK TABLES `rtv3_uid2` WRITE;
/*!40000 ALTER TABLE `rtv3_uid2` DISABLE KEYS */;
INSERT INTO `rtv3_uid2` VALUES ('79ec8f3edc9b62ff387741d3697fd1d837742cf7','N/A','N/A','N/A','{\"kini\": {\"LastUsed\":\"1586817720\",\"UsedTimes\": 2},\"D4V3\": {\"LastUsed\":\"1586844376\",\"UsedTimes\":\"1\"},\"[VU]aXXo\": {\"UsedTimes\":\"1\",\"LastUsed\":\"1586844265\"}}','N/A'),('618cb92c423bab4a6b454c4c8538cd0a389bc7ea','N/A','N/A','N/A','{\"[MK]SahiL\": {\"UsedTimes\": 5,\"LastUsed\":\"1586847605\"}}','N/A'),('2b59e7025f37516a8161ae449be37b9e3f88487e','N/A','N/A','N/A','N/A','N/A'),('8e51624d1ea29b24645dba607da0ae6eb4e9a108','N/A','N/A','N/A','{\"[VU]BliTz\": {\"LastUsed\":\"1586846106\",\"UsedTimes\": 2}}','N/A');
/*!40000 ALTER TABLE `rtv3_uid2` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-14 23:28:54
