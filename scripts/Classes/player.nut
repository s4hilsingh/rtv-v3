class CPlayer
{
	player 			= 	null;
	Registered 	= 	false;
	Logged 			= 	false;
	IsActivated = 1;

	ID 					= 	-1;
	Name 				= 	null;
	OriginIP 			= 	null;
	Password 			= 	null;
	AutoLog			=	false;
	DateRegistered 	= null;
	LastActive			=	null;
	IP 					= "0.0.0.0";

	Stats 			= 	null;
	CurrentStats 	= null;

	TimeJoined	=	1;
	AdminLevel 	= 	0;
	ReadNews 	= 	false;

	Ban 			= 	null;
	Mute 		= 	null;
	RPGBan 	= 	null;
	Warns = 0;

	Language 	= 	"English";

	resultCache 	= 	null;
	voteBase 		= 	null;
	setCount 		= 	0;
	wepSet 		= 	1;
	Assist 			= 	null;
	Round 			= false;
	AssignedTeam = 0;

	SMGSlot1 	= 	0;
	SMGSlot2 	=  0;

	OldName 	=   null;

	FPSWarning 	= 0;
	PingWarning = 0;
	
	Jingles 	= 1;
	Sounds  = 1;
	AutoRespawn = 0;
	SpawnBan = 0;
	SpectateTarget = null;
	
	Freeze = false;
	
	aduty = 0;
	Stealth = 0;
	Mapper = 0;
	
	RobberCoins = 0;

	OldTeam = 0;
	
	Operation = null;
	OperationScore = 0;
	OperationDailyReward = 0;
	OperationVehColor1 = 0;
	OperationVehColor2 = 0;

	ChatType = "old";
	TeamESP = true;

	WinStreak = 0;
	FragileMission = 0;

	MVPSound = 50043;

	NoMVP = false;

	RadioCD = 0;
	
	IsParticipate = false;

	LastPing = null;

	function constructor( player )
	{
		player.Authority = 0;

		this.Stats =
		{
			Kills 	= 0,
			Deaths 	= 0,
			Assist = 0,
			Stealed = 0,
			TopSpree = 0,
			AttWon = 0,
			DefWon = 0,
			MVP = 0,
			XP = 0,
			XPLevel = 0,
			Playtime = 0,
			RoundPlayed = 0,
		}

		this.CurrentStats =
		{
			RoundKills = 0,
			RoundDeaths = 0,
			RoundAssist = 0,
			RoundSpree = 0,
			Playtime = 0,
		}

		this.Assist =
		{
			HitBy = null,
			HitTime = 0,
		}


		this.OldName = player.Name;
		player.Name = "GTA-" + player.ID;

		this.Operation = {};

		this.OperationInit();

		this.LastPing = [];
	}

	function LoadAccount( player )
	{
		try 
		{
			local result = Handler.Handlers.Script.Database.QueryF( "SELECT * FROM rtv3_account INNER JOIN rtv3_pstats ON rtv3_account.ID = rtv3_pstats.ID WHERE Name LIKE '%s';", this.OldName );

			if( result.Step() )
			{
				this.ID = result.GetInteger( "ID" );
				this.OriginIP = result.GetString( "OriginIP" );
				this.DateRegistered = result.GetInteger( "DateReg" );
				this.Password = result.GetString( "Password" );
				this.TimeJoined = result.GetInteger( "Joins" );
				this.Registered = true;
				this.resultCache = result;
				this.IP = player.IP;

				if( ( player.UID == result.GetString( "UID" ) ) && ( player.UID2 == result.GetString( "UID2" ) ) )
				{
					if( !this.verifyBan( player, result ) )
					{
						player.Name = this.OldName;

						this.checkMute( player, result );
						this.checkRPGBan( player, result );

						this.Logged = true;
						this.TimeJoined ++;
						this.LastActive = time();
						this.CurrentStats.Playtime = time();
						this.resultCache = null;

						this.Stats.Kills = result.GetInteger( "Kills" );
						this.Stats.Deaths = result.GetInteger( "Deaths" );
						this.Stats.Assist = result.GetInteger( "Assist" );
						this.Stats.Stealed = result.GetInteger( "Stolen" );
						this.Stats.TopSpree = result.GetInteger( "TopSpree" );
						this.Stats.AttWon = result.GetInteger( "AttWon" );
						this.Stats.DefWon = result.GetInteger( "DefWon" );
						this.Stats.MVP = result.GetInteger( "MVP" );
						this.Stats.RoundPlayed = result.GetInteger( "RoundPlayed" );
						this.Stats.XP = result.GetInteger( "XP" );
						this.Stats.XPLevel = result.GetInteger( "Level" );
						this.Stats.Playtime = result.GetInteger( "Playtime" );
						player.Authority = result.GetInteger( "AdminLevel" );
						this.Mapper = result.GetInteger( "Mapper" );
						this.ReadNews = SToB( result.GetString( "ReadNews" ) );
						this.Sounds = result.GetInteger( "Sounds" );
						this.Jingles = result.GetInteger( "Jingles" );
						this.AutoRespawn = result.GetInteger( "AutoRespawn" );
						this.SpawnBan = result.GetInteger( "SpawnBan" );
						this.RobberCoins = result.GetInteger( "RobberCoins" );
						this.IsActivated = result.GetInteger( "IsActivated" );
						this.Stealth = result.GetInteger( "Stealth" );
					//	this.Operation = 
						this.OperationScore = result.GetInteger( "OperationScore" );
						this.OperationDailyReward = result.GetInteger( "DailyReward" );
						this.OperationVehColor1 = result.GetInteger( "Carcol1" );
						this.OperationVehColor2 = result.GetInteger( "Carcol2" );
						this.ChatType = result.GetString( "ChatMode" );
						this.TeamESP = SToB( result.GetString( "TeamESP" ) );
						this.MVPSound = result.GetInteger( "MVPSound" );
						this.NoMVP = SToB( result.GetString( "NoMVP" ) );

						if ( player.Authority == 0 ) player.Authority = 1;

						if( this.ChatType == "new" ) player.Message( "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" );

						Handler.Handlers.PlayerUID.AddAlias( player );

						if( !this.ReadNews ) Handler.Handlers.Script.sendToClient( player, 2300 );

						Handler.Handlers.Script.sendToClient( player, 2504, this.TeamESP );

						SqCast.MsgAll( "AccJoined",  SqCast.GetPlayerColor( player.Name ), GeoIP.GetDIsplayInfo( player.IP ) );
					
						Handler.Handlers.Discord.ToDiscord( 6, "**%s** joined the server from **%s**.", player.Name, GeoIP.GetDIsplayInfo( player.IP ) );
						
						SqCast.MsgPlr( player, "AccAutoLogged" );
						playsound( player, 50028 ), SqCast.sendAlert( player, "CmdError", "Welcome to Grand Theft Auto!" );

						Handler.Handlers.Script.Database.ExecuteF( "UPDATE rtv3_account SET IsOnline = '1' WHERE ID = '%d';", this.ID );
					
						local teamid = "Defender";
						if( player.Team != Handler.Handlers.Gameplay.Defender ) teamid = "Attacker";
						if( player.Team == 7 ) teamid = "Spectator";

						Handler.Handlers.Operation.LoadData( player, result.GetString( "Missions" ) );

						if( ( time() - this.OperationDailyReward.tointeger() ) > 86400 ) 
						{
							SqCast.MsgPlr(player, "OperationMissionDailyReward");
							Handler.Handlers.Operation.AddXP(player, 50);
							Handler.Handlers.OperationW2.Mission16(player);
							this.OperationDailyReward = time();
						}

						Handler.Handlers.Script.sendToClient( player, 402, SqCast.GetTeamColorOnly( player.Team ) + SqCast.GetTeamName( player.Team ) + " - " + teamid + " - Player(s)  " + Handler.Handlers.Gameplay.getPlayerTeamCountV2( player.Team ) );					
					
						player.MakeTask( function() {
							local getTime = ( time() - player.Data.CurrentStats.Playtime );

							Handler.Handlers.Operation.Mission8( player );

							if( getTime >= 7200 ) Handler.Handlers.OperationW4.Mission29( player );
						}, 3600000, 2 );

						player.GetModuleList();
					}
				}
				else SqCast.MsgPlr( player, "LoginAlert" );
			}
			else Handler.Handlers.Script.sendToClient( player, 700 );
		}
		/*catch( e )
		{
			SqCast.MsgPlr( player, "AccError" );

			player.Kick();
		}*/

		catch( e ) SqLog.Err( "Error on CPlayer::Join [%s]", e );
	}

	function Save( player )
	{
		try 
		{
			if( this.Registered && this.Logged )
			{
				local getTime = ( time() - this.CurrentStats.Playtime );
				local getTotalTime = ( this.Stats.Playtime + getTime );
				local ip = player.IP;

				Handler.Handlers.OperationW2.Mission11( player, getTime );
				Handler.Handlers.OperationW4.Mission26( player, getTime );
				Handler.Handlers.Script.Database.ExecuteF( "UPDATE rtv3_account SET Joins = '%d', UID= '%s', UID2= '%s', IP = '%s', AdminLevel= '%d', Password = '%s', RPGBan = '%s', Mute = '%s', Ban = '%s', DateLog = '%d', RobberCoins = '%d', IsActivated = '%d', Stealth = '%d', AutoRespawn = '%d', Sounds = '%d', IsOnline = '0', ChatMode = '%s', TeamESP = '%s', Mapper = '%d', MVPSound = '%d', NoMVP = '%s'  WHERE ID = '%d';", this.TimeJoined.tointeger(), player.UID, player.UID2, ip, player.Authority, this.Password, ::json_encode( this.RPGBan ), ::json_encode( this.Mute ), ::json_encode( this.Ban ), this.LastActive, this.RobberCoins, this.IsActivated, this.Stealth, this.AutoRespawn, this.Sounds, this.ChatType, this.TeamESP.tostring(), this.Mapper, this.MVPSound, this.NoMVP.tostring(), this.ID );
				Handler.Handlers.Script.Database.ExecuteF( "UPDATE rtv3_pstats SET Kills = '%d', Deaths = '%d', Stolen = '%d', AttWon = '%d', DefWon = '%d', MVP = '%d', TopSpree = '%d', XP = '%d', Level = '%d', Playtime = '%d', ReadNews = '%s', Assist = '%d', RoundPlayed = '%d', Missions = '%s', OperationScore = '%d', DailyReward = '%d', Carcol1 = '%d', Carcol2 = '%d' WHERE ID = '%d';", this.Stats.Kills, this.Stats.Deaths, this.Stats.Stealed, this.Stats.AttWon, this.Stats.DefWon, this.Stats.MVP, this.Stats.TopSpree, this.Stats.XP, this.Stats.XPLevel, getTotalTime, this.ReadNews.tostring(), this.Stats.Assist, this.Stats.RoundPlayed, ::json_encode( this.Operation ), this.OperationScore, this.OperationDailyReward, this.OperationVehColor1, this.OperationVehColor2, this.ID );
			}
		}
		catch( e ) SqLog.Err( "Error on CPlayer::Save [%s]", e );
	}

	function Register( player, password )
	{
		try 
		{
			player.Name = this.OldName;

			this.OriginIP = player.IP;
			this.DateRegistered = time();
			this.Password = SqHash.GetSHA256( password );
			this.Registered = true;
			this.Logged = true;
			this.LastActive = time();
			this.CurrentStats.Playtime = time();		
			player.Authority = 1;

			this.ID = Handler.Handlers.Script.Database.InsertF( "INSERT INTO rtv3_account ( Name, Password, OriginIP, IP, UID, UID2, DateReg, DateLog ) VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d' )", player.Name, this.Password, this.OriginIP, this.OriginIP, player.UID, player.UID2, this.DateRegistered, this.LastActive ).tointeger();
			Handler.Handlers.Script.Database.InsertF( "INSERT INTO rtv3_pstats ( ID ) VALUES ( '%d' )", this.ID );

			Handler.Handlers.PlayerUID.AddAlias( player );
			
			SqCast.MsgAll( "AccJoined1",  SqCast.GetPlayerColor( player.Name ), GeoIP.GetDIsplayInfo( player.IP ) );
			
			SqCast.MsgPlr( player, "RegScs" );
			playsound( player, 50028 ), SqCast.sendAlert( player, "CmdError", "Welcome to Grand Theft Auto!" );
			
			//Handler.Handlers.Discord.ToDiscord( 6, "**%s** joined the server for the first time from **%s**.", player.Name, GeoIP.GetDIsplayInfo( player.IP ) );

			/* send news feed (newly registed) to player */

			local teamid = "Defender";
			if( player.Team != Handler.Handlers.Gameplay.Defender ) teamid = "Attacker";
			if( player.Team == 7 ) teamid = "Spectator";

			//Handler.Handlers.Operation.LoadData( player );

			Handler.Handlers.Script.sendToClient( player, 402, SqCast.GetTeamColorOnly( player.Team ) + teamid + " - Player(s)  " + Handler.Handlers.Gameplay.getPlayerTeamCountV2( player.Team ) );

		}
		catch( e ) SqLog.Err( "Error on CPlayer::Register [%s]", e );
	}

	function Login( player )
	{
		try 
		{
			local result = this.resultCache;

			if( !this.verifyBan( player, result ) )
			{
				player.Name = this.OldName;
				
				this.checkMute( player, result );
				this.checkRPGBan( player, result );

				this.Logged = true;
				this.TimeJoined ++;
				this.LastActive = time();
				this.CurrentStats.Playtime = time();

				this.Stats.Kills = result.GetInteger( "Kills" );
				this.Stats.Deaths = result.GetInteger( "Deaths" );
				this.Stats.Assist = result.GetInteger( "Assist" );
				this.Stats.Stealed = result.GetInteger( "Stolen" );
				this.Stats.TopSpree = result.GetInteger( "TopSpree" );
				this.Stats.AttWon = result.GetInteger( "AttWon" );
				this.Stats.DefWon = result.GetInteger( "DefWon" );
				this.Stats.MVP = result.GetInteger( "MVP" );
				this.Stats.RoundPlayed = result.GetInteger( "RoundPlayed" );
				this.Stats.XP = result.GetInteger( "XP" );
				this.Stats.XPLevel = result.GetInteger( "Level" );
				this.Stats.Playtime = result.GetInteger( "Playtime" );
				player.Authority = result.GetInteger( "AdminLevel" );
				this.Mapper = result.GetInteger( "Mapper" );				
				this.ReadNews = SToB( result.GetString( "ReadNews" ) );
				this.Sounds = result.GetInteger( "Sounds" );
				this.Jingles = result.GetInteger( "Jingles" );
				this.AutoRespawn = result.GetInteger( "AutoRespawn" );
				this.SpawnBan = result.GetInteger( "SpawnBan" );
				this.RobberCoins = result.GetInteger( "RobberCoins" );
				this.IsActivated = result.GetInteger( "IsActivated" );
				this.Stealth = result.GetInteger( "Stealth" );
			//	this.Operation = result.GetString( "Missions" );
				this.OperationScore = result.GetInteger( "OperationScore" );
				this.OperationDailyReward = result.GetInteger( "DailyReward" );
				this.OperationVehColor1 = result.GetInteger( "Carcol1" );
				this.OperationVehColor2 = result.GetInteger( "Carcol2" );
				this.ChatType = result.GetString( "ChatMode" );
				this.TeamESP = SToB( result.GetString( "TeamESP" ) );
				this.MVPSound = result.GetInteger( "MVPSound" );
				this.NoMVP = SToB( result.GetString( "NoMVP" ) );

				if ( player.Authority == 0 ) player.Authority = 1;		
						
				Handler.Handlers.PlayerUID.AddAlias( player );

				if( !this.ReadNews ) Handler.Handlers.Script.sendToClient( player, 2300 );
				Handler.Handlers.Script.sendToClient( player, 2504, this.TeamESP );

				if( this.ChatType == "new" ) player.Message( "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" );

				if(player.UID == "45dc56d324df443267f100463192902293e1f7fa") SqCast.MsgAll( "AccJoined",  SqCast.GetPlayerColor( player.Name ), "Mars, Solar System" );
				else SqCast.MsgAll( "AccJoined", SqCast.GetPlayerColor( player.Name ), GeoIP.GetDIsplayInfo( this.IP ) );
				
				SqCast.MsgPlr( player, "LogScs" );
				playsound( player, 50028 ), SqCast.sendAlert( player, "CmdError", "Welcome to Grand Theft Auto!" );
				
				Handler.Handlers.Discord.ToDiscord( 6, "**%s** joined the server from **%s**.", player.Name, GeoIP.GetDIsplayInfo( player.IP ) );
			
				Handler.Handlers.Script.Database.ExecuteF( "UPDATE rtv3_account SET IsOnline = '1' WHERE ID = '%d';", this.ID );

				local teamid = "Defender";
				if( player.Team != Handler.Handlers.Gameplay.Defender ) teamid = "Attacker";
				if( player.Team == 7 ) teamid = "Spectator";

				Handler.Handlers.Operation.LoadData( player, result.GetString( "Missions" ) );

				if( ( time() - this.OperationDailyReward.tointeger() ) > 86400 ) 
				{
					SqCast.MsgPlr(player, "OperationMissionDailyReward");
					Handler.Handlers.Operation.AddXP(player, 50);
					Handler.Handlers.OperationW2.Mission16(player);

					this.OperationDailyReward = time();
				}

				Handler.Handlers.Script.sendToClient( player, 402, SqCast.GetTeamColorOnly( player.Team ) + SqCast.GetTeamName( player.Team ) + " - " + teamid + " - Player(s)  " + Handler.Handlers.Gameplay.getPlayerTeamCountV2( player.Team ) );
			
				player.MakeTask( function() {
					local getTime = ( time() - player.Data.CurrentStats.Playtime );

					Handler.Handlers.Operation.Mission8( player );

					if( getTime >= 7200 ) Handler.Handlers.OperationW4.Mission29( player );
				}, 3600000, 1 );

				player.GetModuleList();
			}

			this.resultCache = null;
		}	

		catch( e ) SqLog.Err( "Error on CPlayer::Login [%s]", e );
	}

	function verifyBan( player, result )
	{
		try 
		{
			this.Ban = ::json_decode( result.GetString( "Ban" ) );
			if( this.Ban )
			{
	            if( this.Ban.Duration.tointeger() > ( time() - this.Ban.Time.tointeger() ) )
	            {
					SqCast.MsgPlr( player, "Kickban", SqCast.GetPlayerColor( this.Ban.Admin ), this.Ban.Reason, GetDate( this.Ban.Time.tointeger() ) );
					SqCast.MsgPlr( player, "KickbanTimered", GetTiming( ( this.Ban.Duration.tointeger() - ( time() - this.Ban.Time.tointeger() ) ) ) );

	                if( this.Ban.Duration.tointeger() > 604800 ) {}/* perma ban msg */

	                player.Kick();

	                return true;
	            }

				else 
				{
					this.Ban = null;
				}
			}
		}
		catch( e ) SqLog.Err( "Error on CPlayer::verifyBan [%s]", e );
	}

	function checkMute( player, result )
	{
		try 
		{
			if( !this.Mute )
			{
				this.Mute = ::json_decode( result.GetString( "Mute" ) );
				if( this.Mute )
				{
			        if( this.Mute.Duration.tointeger() > ( time() - this.Mute.Time.tointeger() ) )
			        {
		                player.MakeTask( function()
		                {  
		                    player.Data.Mute = null;
		                                                            
		                   	this.Terminate();

		                }, ( player.Data.Mute.Duration.tointeger() * 1500 ), 1 ).SetTag( "Mute" );


			           if( this.Mute.Duration.tointeger() > 604800 ) {}/* shun msg */
			        }

					else 
					{
		                this.Mute = null;
					}						
				}
			}
		}
		catch( e ) SqLog.Err( "Error on CPlayer::checkMute [%s]", e );
	}

	function checkRPGBan( player, result )
	{
		try 
		{
			if( !this.RPGBan )
			{
				this.RPGBan = ::json_decode( result.GetString( "RPGBan" ) );
				if( this.RPGBan )
				{
			        if( this.RPGBan.Duration.tointeger() > ( time() - this.RPGBan.Time.tointeger() ) )
			        {
		                player.MakeTask( function()
		                {  
		                    player.Data.Mute = null;
		                                                            
		                   	this.Terminate();

		                }, ( player.Data.RPGBan.Duration.tointeger() * 1500 ), 1 ).SetTag( "RPGBan" );


			           if( this.RPGBan.Duration.tointeger() > 604800 ) {}/* perma rpgban msg */
			        }

					else 				
					{
						this.RPGBan = null;
					}						
				}
			}
		}
		catch( e ) SqLog.Err( "Error on CPlayer::checkRPGBan [%s]", e );
	}

	function InRound( player )
	{
		try 
		{
			if( player.Team == 1 || player.Team == 2 && player.Spawned ) return player.Spawned;
		}
		catch( e ) SqLog.Err( "Error on CPlayer::InRound [%s]", e );

	}

	function OperationInit()
	{
            

            this.Operation.rawset("Mission1", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission2", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission3", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission4", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission5", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission6", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission7", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission8", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission9", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission10", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission11", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission12", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission13", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission14", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission15", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission16", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission17", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission18", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission19", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission20", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission21", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission22", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission23", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission24", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission25", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission26", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission27", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission28", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission29", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission30", {
                Progress = 0,
                DateComplete = 0,
            });

            this.Operation.rawset("Mission31", {
                Progress = 0,
                DateComplete = 0,
            });
	}
}