SqCommand.Create( "forum", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
	if( player.Data.Logged )
    {
		SqCast.MsgPlr( player, "ShowForum", "" );
    }
    else SqCast.MsgPlr( player, "ErrCmd" );

	return true;
});

SqCommand.Create( "discord", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
	if( player.Data.Logged )
    {
		SqCast.MsgPlr( player, "ShowDiscord" );
    }
    else SqCast.MsgPlr( player, "ErrCmd" );

	return true;
});

SqCommand.Create( "credits", "", [ "" ], 0, 0, -1, true ).BindExec( this, function( player, args )
{
	if( player.Data.Logged )
    {
		SqCast.MsgPlr( player, "ShowCreditsHeader" );
		SqCast.MsgPlr( player, "ShowCreditsMain" );
		SqCast.MsgPlr( player, "ShowCreditsDev" );
		SqCast.MsgPlr( player, "ShowCreditsUI" );
		SqCast.MsgPlr( player, "ShowCreditsSpecial" );
		SqCast.MsgPlr( player, "ShowCreditPanel" );
    }
    else SqCast.MsgPlr( player, "ErrCmd" );

	return true;
});

SqCommand.Create( "cmds", "g", [ "Text" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
	if( player.Data.Logged )
    {
		if( args.rawin( "Text" ) )
		{
			switch( args.Text )
			{
				case "basic":
				SqCast.MsgPlr( player, "ShowCmds" );
				SqCast.MsgPlr( player, "ShowKeyBinds" );
				
				if( player.Authority > 1 ) SqCast.MsgPlr( player, "ShowCmdAdmin" );
				break;

				case "operation":
				case "op":
				SqCast.MsgPlr( player, "ShowOperationCmds" );
				break;

				default:
				SqCast.MsgPlr( player, "CmdsSyntax" );
				break;
			}
		}
		else SqCast.MsgPlr( player, "CmdsSyntax" );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

	return true;
});

