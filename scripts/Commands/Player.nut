SqCommand.Create( "stats", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            local ratio = ( typeof( player.Data.Stats.Kills.tofloat() / player.Data.Stats.Deaths.tofloat() ) != "float" ) ? 0.0 : player.Data.Stats.Kills.tofloat() / player.Data.Stats.Deaths.tofloat();

            SqCast.MsgPlr( player, "OwnStats" );
            SqCast.MsgPlr( player, "OwnStats1", player.Data.Stats.Kills, player.Data.Stats.Deaths, ratio, ( ( player.Data.Stats.TopSpree >= 5 ) ? 0 : player.Data.Stats.TopSpree ) );
            SqCast.MsgPlr( player, "OwnStats2", player.Data.Stats.RoundPlayed, player.Data.Stats.AttWon, player.Data.Stats.DefWon, player.Data.Stats.MVP, player.Data.Stats.Stealed );
        }

        else 
        {
            local target = Handler.Handlers.Script.FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
                        local ratio = ( typeof( target.Data.Stats.Kills.tofloat() / target.Data.Stats.Deaths.tofloat() ) != "float" ) ? 0.0 : target.Data.Stats.Kills.tofloat() / target.Data.Stats.Deaths.tofloat();

                        SqCast.MsgPlr( player, "TargetStats", SqCast.GetPlayerColor( target ) );
                        SqCast.MsgPlr( player, "OwnStats1", target.Data.Stats.Kills, target.Data.Stats.Deaths, ratio, ( ( target.Data.Stats.TopSpree >= 5 ) ? 0 : target.Data.Stats.TopSpree ) );
                        SqCast.MsgPlr( player, "OwnStats2", target.Data.Stats.RoundPlayed, target.Data.Stats.AttWon, target.Data.Stats.DefWon, target.Data.Stats.MVP, target.Data.Stats.Stealed );
                    }

                    else 
                    {
                        local ratio = ( typeof( player.Data.Stats.Kills.tofloat() / player.Data.Stats.Deaths.tofloat() ) != "float" ) ? 0.0 : player.Data.Stats.Kills.tofloat() / player.Data.Stats.Deaths.tofloat();

                        SqCast.MsgPlr( player, "OwnStats" );
                        SqCast.MsgPlr( player, "OwnStats1", player.Data.Stats.Kills, player.Data.Stats.Deaths, ratio, ( ( player.Data.Stats.TopSpree >= 5 ) ? 0 : player.Data.Stats.TopSpree ) );
                        SqCast.MsgPlr( player, "OwnStats2", player.Data.Stats.RoundPlayed, player.Data.Stats.AttWon, player.Data.Stats.DefWon, player.Data.Stats.MVP, player.Data.Stats.Stealed );
                    }
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "hp", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            SqCast.MsgPlr( player, "ShowHPSelf", player.Health+player.Armour );
        }
        else 
        {
            local target = FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
                        SqCast.MsgPlr( player, "ShowHP", SqCast.GetPlayerColor( target ), target.Health+target.Armour );
                    }
                    else SqCast.MsgPlr( player, "ShowHPSelf", player.Health+player.Armour );
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "report", "s|g", [ "Target", "Reason" ], 0, 2, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( args.rawin( "Target" ) )
		{
			if( args.rawin( "Reason" ) )
			{
				local target = FindPlayer( args.Target );
				if( target )
				{
					if ( target.ID != player.ID )
					{					
                        SqCast.MsgAllAdmin( "ReportAdmin", player.Name, target.Name, args.Reason );
                        SqCast.MsgPlr( player, "ReportSubmit");
                    }
					else SqCast.MsgPlr( player, "CantReportSelf" );
				}
				else SqCast.MsgPlr( player, "TargetXOnline");
			}
			else SqCast.MsgPlr( player, "ReportSyntax");
		}
        else SqCast.MsgPlr( player, "ReportSyntax");
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "fps", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            SqCast.MsgPlr( player, "ShowFPSPingSelf", player.Ping, player.FPS );
        }
        else 
        {
            local target = Handler.Handlers.Script.FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
                        SqCast.MsgPlr( player, "ShowFPSPing", SqCast.GetPlayerColor( target ), target.Ping, target.FPS );
                    }
                    else SqCast.MsgPlr( player, "ShowFPSPingSelf", player.Ping, player.FPS );
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "ping", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            SqCast.MsgPlr( player, "ShowFPSPingSelf", player.Ping, player.FPS );
        }

        else 
        {
            local target = Handler.Handlers.Script.FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
                        SqCast.MsgPlr( player, "ShowFPSPing", SqCast.GetPlayerColor( target ), target.Ping, target.FPS );
                    }
                    else SqCast.MsgPlr( player, "ShowFPSPingSelf", player.Ping, player.FPS );
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "changepass", "s|s", [ "OldPass", "NewPass" ], 0, 2, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( args.rawin( "OldPass" ) )
        {
            if( args.rawin( "NewPass" ) )
            {
                if( player.Data.Password == SqHash.GetSHA256( args.OldPass ) )
                {
                    if( args.NewPass.len() > 4 )
                    {
                        if( args.OldPass.tolower() != args.NewPass.tolower() )
                        {
                            player.Data.Password = SqHash.GetSHA256( args.NewPass );

                            SqCast.MsgPlr( player, "Changepass" );
                        }
                        else SqCast.MsgPlr( player, "ChangepassOldSameNew" );
                    }
                    else SqCast.MsgPlr( player, "ChangepassNewPassNotEnough" );
                }
                else SqCast.MsgPlr( player, "ChangepassOldPass" );
            }
            else SqCast.MsgPlr( player, "ChangepassSyntax" );
        }
        else SqCast.MsgPlr( player, "ChangepassSyntax" );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "country", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            SqCast.MsgPlr( player, "SelfCountry", GeoIP.GetDIsplayInfo( player.IP ) );
        }

        else 
        {
            local target = Handler.Handlers.Script.FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
                        SqCast.MsgPlr( player, "TargetCountry", SqCast.GetPlayerColor( target ), GeoIP.GetDIsplayInfo( target.IP ) );
                    }
                    else SqCast.MsgPlr( player, "SelfCountry", GeoIP.GetDIsplayInfo( player.IP ) );
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "admins", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
	local adminlist=GetAdminList();
    if( player.Data.Logged )
    {	
		if ( adminlist == "" ) SqCast.MsgPlr( player, "NoAdmins" );
		else SqCast.MsgPlr( player, "Admins", adminlist );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

/*SqCommand.Create( "jingles", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
		if ( player.Data.Jingles == 0 ) 
		{
			player.Data.Jingles=1;
			player.Message("Jingles have been enabled.");
			Handler.Handlers.Script.Database.ExecuteF( "UPDATE rtv3_account SET Jingles = 1 WHERE Name = '%s';", player.Name );
			PlayJingles( player );
		}
		else
		{
		player.Data.Jingles = 0;
		player.Message("Jingles have been disabled, alt-tab out and in back to game to stop the jingles playing already.");
		Handler.Handlers.Script.Database.ExecuteF( "UPDATE rtv3_account SET Jingles = 0 WHERE Name = '%s';", player.Name );
		}
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});*/

SqCommand.Create( "rules", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
        SqCast.MsgPlr( player, "ListRules1" );
        SqCast.MsgPlr( player, "ListRules2" );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "help", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
        SqCast.MsgPlr( player, "HelpMsg" );
        SqCast.MsgPlr( player, "HelpMsg1" );
        SqCast.MsgPlr( player, "HelpMsg2" );
        SqCast.MsgPlr( player, "HelpMsg3" );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "sounds", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
		if ( player.Data.Sounds == 0 ) 
		{
			player.Data.Sounds = 1;

            SqCast.MsgPlr( player, "SoundsEnable" );

			PlayJingles( player );
		}
		else
		{
            player.Data.Sounds = 0;

            SqCast.MsgPlr( player, "SoundsDisable" );
		}
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "players", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
		local attackers = 0, defenders = 0, spectators = 0, defcolor = "", attcolor = "";
		local reds, blues;
		reds = Handler.Handlers.Gameplay.getPlayerTeamCount( 1 );
		blues = Handler.Handlers.Gameplay.getPlayerTeamCount( 2 );
		if ( Handler.Handlers.Gameplay.Defender != 1 )
		{
			attackers = Handler.Handlers.Gameplay.getPlayerTeamCountV3( 1 );
			attcolor = "Red Team";
			defenders = Handler.Handlers.Gameplay.getPlayerTeamCountV3( 2 ); 
			defcolor = "Blue Team";
		}
		else
		{
			defenders = Handler.Handlers.Gameplay.getPlayerTeamCountV3( 1 );
			attcolor = "Red Team";
			attackers = Handler.Handlers.Gameplay.getPlayerTeamCountV3( 2 );
			defcolor = "Blue Team";
		}
		spectators = Handler.Handlers.Gameplay.getPlayerTeamCountV3( 7 );

		SqCast.MsgPlr( player, "GetPlayers", GetPlayers(), reds, blues, spectators );
        if( Handler.Handlers.Gameplay.Status > 2 ) SqCast.MsgPlr( player, "RoundActiveGetPlr", attcolor, attackers.tointeger(), defcolor, defenders.tointeger(), spectators );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "world", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            SqCast.MsgPlr( player, "OwnWorld", player.World );
        }
        else 
        {
            local target = Handler.Handlers.Script.FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
                       SqCast.MsgPlr( player, "TargetWorld", target.Name, target.World );
                    }
                    else SqCast.MsgPlr( player, "OwnWorld", player.World );
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "level", "g", [ "Target" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( !args.rawin( "Target" ) )
        {
            SqCast.MsgPlr( player, "OwnLevel", player.Authority, GetRank( player.Authority ) );
        }
        else 
        {
            local target = Handler.Handlers.Script.FindPlayer( args.Target );
            if( target )
            {
                if( target.Data.Logged )
                {
                    if( target.ID != player.ID )
                    {
						if ( target.Data.Stealth == 1 ) SqCast.MsgPlr( player, "TargetLevel", target.Name, target.Authority, "Player" );
                        else SqCast.MsgPlr( player, "TargetLevel", target.Name, target.Authority, GetRank( target.Authority ) );
                    }

                    else SqCast.MsgPlr( player, "OwnLevel", player.Authority, GetRank( player.Authority ) );
                }
                else SqCast.MsgPlr( player, "TargetXOnline" );
            }
            else SqCast.MsgPlr( player, "TargetXOnline" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "missions", "s", [ "Text" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
        if( args.rawin( "Text" ) )
        {
            switch( args.Text )
            {
                case "1":
                SqCast.MsgPlr( player, "OperationWeek", 1 );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 500 Headshots", 200, ( player.Data.Operation["Mission1"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission1"]["Progress"] + " / 500" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Deliver 50 Vehicles" 200, ( player.Data.Operation["Mission2"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission2"]["Progress"] + " / 50" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Win 100 Round as attacker" 200, ( player.Data.Operation["Mission3"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission3"]["Progress"] + " / 100" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 150 Kills with shotgun" 200, ( player.Data.Operation["Mission4"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission4"]["Progress"] + " / 150" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 5 Spree in single round" 50, ( player.Data.Operation["Mission5"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission5"]["Progress"] + " / 5" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 1 MVP" 50, ( player.Data.Operation["Mission6"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission6"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Win a round as defender" 50, ( player.Data.Operation["Mission7"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission7"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Play at least 1 hour" 50, ( player.Data.Operation["Mission8"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission8"]["Progress"] + " / 1" );
                break;

                case "2":
                SqCast.MsgPlr( player, "OperationWeek", 2 );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 50 kills with Chainsaw", 200, ( player.Data.Operation["Mission9"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission9"]["Progress"] + " / 50" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Finish 5 matchs with 10 kills or above" 200, ( player.Data.Operation["Mission10"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission10"]["Progress"] + " / 5" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get total 6 hours playtime" 200, ( player.Data.Operation["Mission11"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission11"]["Progress"] + " / 21600" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Kill player from 50 meters or above 100 times" 200, ( player.Data.Operation["Mission12"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission12"]["Progress"] + " / 100" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 5 win as Defender" 50, ( player.Data.Operation["Mission13"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission13"]["Progress"] + " / 5" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Win a round with 0 dead" 50, ( player.Data.Operation["Mission14"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission14"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Use any operation exclusive command 1 time" 50, ( player.Data.Operation["Mission15"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission15"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Collect daily reward 5 times" 50, ( player.Data.Operation["Mission16"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission16"]["Progress"] + " / 5" );
                break;

                case "3":
                SqCast.MsgPlr( player, "OperationWeek", 3 );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 500 kills with Ruger", 200, ( player.Data.Operation["Mission17"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission17"]["Progress"] + " / 500" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Win 100 as defender" 200, ( player.Data.Operation["Mission18"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission18"]["Progress"] + " / 100" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 300 headshots with any sniper rifles" 200, ( player.Data.Operation["Mission19"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission19"]["Progress"] + " / 300" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 5 win streaks in single session" 200, ( player.Data.Operation["Mission20"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission20"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Pick health or armour pickup which spawned by player" 50, ( player.Data.Operation["Mission21"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission21"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 5 kills with RPG" 50, ( player.Data.Operation["Mission22"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission22"]["Progress"] + " / 5" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Spawn as spectator" 50, ( player.Data.Operation["Mission23"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission23"]["Progress"] + " / 1" );
            //    SqCast.MsgPlr( player, "OperationMissionDes", "Collect daily reward 5 times" 50, ( player.Data.Operation["Mission16"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission16"]["Progress"] + " / 5" );
                break;

                case "4":
                SqCast.MsgPlr( player, "OperationWeek", 4 );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 500 kills with M4", 200, ( player.Data.Operation["Mission24"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission24"]["Progress"] + " / 500" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get 15 MVP" 200, ( player.Data.Operation["Mission25"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission25"]["Progress"] + " / 15" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Get total 15 hours playtime" 200, ( player.Data.Operation["Mission26"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission26"]["Progress"] + " / 54000" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Kill player below 5 meteres 250 times" 200, ( player.Data.Operation["Mission27"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission27"]["Progress"] + " / 250" );
                SqCast.MsgPlr( player, "OperationMissionDes", "15 players or above in server bonus" 100, ( player.Data.Operation["Mission28"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission28"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Play at least 2 hours" 50, ( player.Data.Operation["Mission29"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission29"]["Progress"] + " / 1" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Kill any staff member 50 times" 50, ( player.Data.Operation["Mission30"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission30"]["Progress"] + " / 50" );
                SqCast.MsgPlr( player, "OperationMissionDes", "Kill " + Handler.Handlers.PlayerAccount.GetAccountNameFromID( 616 ) + " 5 times without dying" 50, ( player.Data.Operation["Mission31"]["DateComplete"].tointeger() != 0 ) ? "Completed" : player.Data.Operation["Mission31"]["Progress"] + " / 1" );
                break;

                default:
                SqCast.MsgPlr( player, "MissionSyntax" );
                break;
            }
        }
        else SqCast.MsgPlr( player, "MissionSyntax" );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "opscore", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
        SqCast.MsgPlr( player, "OperationScore", Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ), player.Data.OperationScore, Handler.Handlers.Operation.getExperienceAtLevel( ( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) + 1 ) ) );
        
        if( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) >= 5 ) SqCast.MsgPlr( player, "OperationReward", "Able to add text in lobby (use /doodle)" );
        if( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) >= 10 )SqCast.MsgPlr(player, "OperationReward", "Change to cutomize color when entering vehicle (use /carcol to customize)");
        if( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) >= 20 )SqCast.MsgPlr(player, "OperationReward", "Ability to spawn health pickup (use /spawnhp)");
        if( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) >= 30 )SqCast.MsgPlr(player, "OperationReward", "Ability to spawn health pickup (use /spawnarmour)");

    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "doodle", "g", [ "Text" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) >= 5 )
        {
            if( ( Handler.Handlers.PlayerUID.CheckMute( player ) ) )
            {
                if( args.rawin( "Text" ) )
                {
                    local id = Handler.Handlers.Script.DoddleBoardPos;

                    Handler.Handlers.Script.DoddleBoard[ id ].Text = StripCol( args.Text );
                    Handler.Handlers.Script.DoddleBoard[ id ].Author = player.Name;

                    Handler.Handlers.Script.sendToClientToAll( 2201, id + "`" + StripCol( args.Text ) + "`" + player.Name );
                    Handler.Handlers.Script.sendToClientToAllExpPlr( player, 2200, id + "`" + StripCol( args.Text ) + "`" + player.Name );

                    Handler.Handlers.Script.DoddleBoardPos ++;

                    SqCast.MsgPlr( player, "DoodleScs" );

                    if( Handler.Handlers.Script.DoddleBoardPos >= 11 ) Handler.Handlers.Script.DoddleBoardPos = 1;

                    Handler.Handlers.OperationW2.Mission15( player );
                }
                else SqCast.MsgPlr( player, "DoodleSyntax" );
            }
            else SqCast.MsgPlr( player, "CantTalkMuted" );
        }
        else SqCast.MsgPlr( player, "OperationCommandLevelNotEnough", 5 );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "carcol", "s|s", [ "Col1", "Col2" ], 0, 2, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {
        if( Handler.Handlers.Operation.getLevelAtExperience( player.Data.OperationScore ) >= 10 )
        {
            if( args.rawin( "Col1" ) &&  args.rawin( "Col2" ) )
            {
                if( IsNum( args.Col1 ) && IsNum( args.Col2 ) )
                {
                    player.Data.OperationVehColor1 = args.Col1.tointeger();
                    player.Data.OperationVehColor2 = args.Col2.tointeger();

                    SqCast.MsgPlr( player, "CarcolUpdated" );

                    Handler.Handlers.OperationW2.Mission15( player );
                }
                else SqCast.MsgPlr( player, "CarcolNotNum" );
            }
            else SqCast.MsgPlr( player, "CarcolSyntax" );
        }
        else SqCast.MsgPlr( player, "OperationCommandLevelNotEnough", 10 );
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "changemvpsound", "s", [ "Text" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
        if( args.rawin( "Text" ) )
        {
            switch( args.Text )
            {
                case "1":
                player.Data.MVPSound = 50043;

                SqCast.MsgPlr( player, "MVpChange", "Dancing" );
                break;

                case "2":
                player.Data.MVPSound = 50044;

                SqCast.MsgPlr( player, "MVpChange", "EZ KATKA" );
                break;
                
                case "3":
                player.Data.MVPSound = 50045;

                SqCast.MsgPlr( player, "MVpChange", "WOAH" );
                break;
                
                case "4":
                player.Data.MVPSound = 50047;

                SqCast.MsgPlr( player, "MVpChange", "EZ4ENCE" );
                break;
                
                case "5":
                player.Data.MVPSound = 50046;

                SqCast.MsgPlr( player, "MVpChange", "Nico Nico Nii" );
                break;

                case "6":
                player.Data.MVPSound = 50048;

                SqCast.MsgPlr( player, "MVpChange", "SA Mission Passed" );
                break;

                case "7":
                player.Data.MVPSound = 50049;

                SqCast.MsgPlr( player, "MVpChange", "SOSAT BLYAT" );
                break;

                default:
                SqCast.MsgPlr( player, "MVPSyntax" );
                SqCast.MsgPlr( player, "MVPSyntax1" );
            }
        }
        else 
        {
            SqCast.MsgPlr( player, "MVPSyntax" );
            SqCast.MsgPlr( player, "MVPSyntax1" );
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});

SqCommand.Create( "playsound", "g", [ "ID" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
	if( args.rawin( "ID" ) )
	{
		try
		{
			playsound( player, args.ID.tointeger() );

			SqCast.MsgPlr( player, "PlaySoundX", args.ID.tointeger() );
		}
		catch( e ) SqCast.MsgPlr( player, "ErrPlaySound", e );
	}
	else SqCast.MsgPlr( player, "PlaySoundSyntax1" );

    return true;
});

SqCommand.Create( "login", "s", [ "ID" ], 0, 1, -1, true, true ).BindExec( this, function( player, args )
{
	if( args.rawin( "ID" ) )
	{
		if( player.Data.Registered )
		{
			if( !player.Data.Logged )
			{
				if( player.Data.Password == SqHash.GetSHA256( args.ID ) )
				{
					player.Data.Login( player );

					Handler.Handlers.Script.sendToClient( player, 601 );
				}
				else SqCast.MsgPlr( player, "LoginXPassword" );
            }
            else SqCast.MsgPlr( player, "LoginXPassword" );
        }
        else SqCast.MsgPlr( player, "NotReg" );
    }
    else SqCast.MsgPlr( player, "LoginSyntax" );

    return true;
});

SqCommand.Create( "disablemvp", "", [ "" ], 0, 0, -1, true, true ).BindExec( this, function( player, args )
{
    if( player.Data.Logged )
    {	
        switch( player.Data.NoMVP )
        {
            case true:
            player.Data.NoMVP = false;
            SqCast.MsgPlr( player, "EnableMVP" );
            break;

            case false:
            player.Data.NoMVP = true;
            SqCast.MsgPlr( player, "DisableMVP" );
            break;
        }
    }
    else SqCast.MsgPlr( player, "ErrCmdNoLog" );

    return true;
});
