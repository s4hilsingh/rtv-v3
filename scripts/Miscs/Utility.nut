function RGBToHex( color )
{
	return format("[#%02X%02X%02X]", color.r, color.g, color.b );
}

function GetPlayer( player )
{
	if( player )
	{
		if( IsNum( player ) )
		{
			local n = FindPlayer( player.tointeger() );
			if ( n ) return n;
		}
		
		else 
		{      
			local b = FindPlayer( player ); 
			if ( b ) return b;
		}
	}
}

function ConvertToPos( str )
{
	local strip = split( str, "," );

	return Vector( strip[0].tofloat(), strip[1].tofloat(), strip[2].tofloat() );
}

function ConvertToStrPos( pos )
{
	return pos.x + ", " + pos.y + ", " + pos.z;
}

function EscapeJSONString( string )
{
	local getInvalidChar = [ "\"", "'", "\\", ":" ]

	if( !string ) return;

	foreach( index, value in getInvalidChar )
	{
		if( string.find( value ) >= 0 ) string = SearchAndReplace( string, value, " " );
	}

	return string;
}

function SearchAndReplace( str, search, replace ) 
{
    local li = 0, ci = str.find( search, li ), res = "";
    while( ci != null ) 
    {
        if( ci > 0 ) 
        {
            res += str.slice( li, ci );
            res += replace;
        }
        else res += replace;
        li = ci + search.len(), ci = str.find( search, li );
    }
    if ( str.len() > 0 ) res += str.slice( li );
    return res;
}

function IsFloat( num )
{
	try
	{
		local a = num.tofloat();
		if( typeof a == "float" ) return true;
	}
	catch( _ ) _;

	return false;
}

function GetTok( string, separator, n, ... )
{
	local m = vargv.len() > 0 ? vargv[0] : n,
	tokenized = split( string, separator ),
	text = "";

	if ( n > tokenized.len() || n < 1 ) return null;
	for( ; n <= m; n++ )
	{
		text += text == "" ? tokenized[n-1] : separator + tokenized[n-1];
	}
	return text;
}

function NumTok( string, separator )
{
	local tokenized = split( string, separator );
	return tokenized.len();
}

function GetTiming( secs )
{
	local ret = "";
	local hr = 0;
	local mn = 0;
	local dy = 0;

	mn = secs / 60;
	secs = secs - mn*60;
	hr = mn / 60;
	mn = mn - hr*60;
	dy = hr / 24;
	hr = hr - dy*24

	if ( dy > 0 ) ret = dy + " Days ";
	if ( hr > 0 ) ret = ret + hr + " Hours ";
	if ( mn > 0 ) ret = ret + mn + " Minutes ";
	ret = ret + secs +" Seconds";

	return ret;
}

function GetDate( time )
{
	return date( time ).day + "/" + ( date( time ).month + 1 ) + "/" + date( time ).year + " " + format( "%02d", date( time ).hour ) + ":" + format( "%02d", date( time ).min );
}

function ConvertToRGBA( str )
{
	local strip = split( str, "," );

	return RGBA( strip[0].tointeger(), strip[1].tointeger(), strip[2].tointeger(), 255 );
}

function GetFrontPosition(position, angle, gap) return Vector(position.x - gap * sin(angle), position.y + gap * cos(angle), position.z); 

function IsFloat( num )
{
	try
	{
		if( typeof num.tofloat() == "float" ) return true;
	}
	catch( e ) return false;
}
	
function IsNum( num )
{
	try
	{
		num = num.tointeger();
		if( typeof num == "integer" ) return true;
	}
	catch( e ) return false;
}

function accurate_seed() {
    local uptime = split(clock().tostring(), ".");
    uptime = uptime.len() > 1 ? uptime[0] + uptime[1] : uptime[0];
    return uptime.tointeger();
}

function CalculateAvg(arr) {
    local sum = 0, count = 0;
    foreach(entry in arr) {
        sum += entry;
        count++;
    }

    return sum.tofloat() / count.tofloat();
}

function CalculateVariance(arr) {
    local avg = CalculateAvg(arr);
    local sum = 0, count = 0;

    foreach (entry in arr) {
        sum += pow(entry - avg, 2);
        count++;
    }

    return sum.tofloat() / count.tofloat();
}

function CalculateJitter(arr) {
    return sqrt(CalculateVariance(arr));
}