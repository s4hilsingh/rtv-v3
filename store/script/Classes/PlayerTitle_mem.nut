class CPTitle extends CContext
{
	Players = null;
	IsSpec = false;
	TeamESP = true;
	Team = "255";

	function constructor( Key )
	{
		base.constructor();

		this.Players = {};

		this.Load();
	}

	function AddPlayer( player, text )
	{
		local element = GUILabel();
		
		element.Alpha = 255;
		element.Text = text;
		element.TextColour = Colour( 255, 255, 255 );
		element.FontSize = ( GUI.GetScreenSize().X * 0.0073206442166911 );
		element.TextAlignment = GUI_ALIGN_LEFT;
		element.FontFlags = GUI_FFLAG_BOLD | GUI_FFLAG_OUTLINE;

		this.Players.rawset( player, 
		{
			Element = element,
			Team = "0",
			HitWarn = 0,
		});
	}

	function UpdatePlayerColor( id, r, g, b, team )
	{
		this.Players[ id ].Element.TextColour = Colour( r, g, b );
		this.Players[ id ].Team = team;
		this.Players[ id ].HitWarn = 0;
	}

	function Load() 
	{
		for( local i = 0; i < 100; i++ )
		{
			AddPlayer( i, "" );
		}
	}

	function onScriptProcess() 
	{
		local lplayer = World.FindLocalPlayer(), sender;
		local lpos = lplayer.Position, spos;
		local lbl, wpts, z1 = Vector( 0, 0, 0.5 );
		
		foreach( index, value in this.Players )
		{
			if( this.IsSpec )
			{
				if( sender = index && World.FindPlayer( index ) )
				{
					if( World.FindPlayer( index ).Name.find("GTA-") == null )
					{
						spos = sender.Position, lbl = value.Element, lbl.Alpha = 0;

						if( ::Distance( lpos.X, lpos.Y, lpos.Z, spos.X, spos.Y, spos.Z ) < 15000 )
						{
							wpts = GUI.WorldPosToScreen( spos + z1 );
							
							lbl.Position = VectorScreen( wpts.X, wpts.Y );
							
							local ChatBubble_PosToScreen = GUI.WorldPosToScreen( spos );
							if( ChatBubble_PosToScreen.Z < 1 )
							{
								lbl.Alpha = 255;
								lbl.Text = World.FindPlayer( index ).Name;
							}
							else
							{
								lbl.Alpha = 0;
							}
						}
								
						if( sender == lplayer )
						{
							lbl.Alpha = 0;
						}
					}
					else value.Element.Text = "";
				}
				else value.Element.Text = "";
			}

			else 
			{
				if( this.TeamESP )
				{
					if( this.Team != "255" )
					{
						if( sender = index && World.FindPlayer( index ) )
						{
							if( World.FindPlayer( index ).Name.find("GTA-") == null )
							{
								if( value.Team == this.Team )
								{
									spos = sender.Position, lbl = value.Element, lbl.Alpha = 0;

									if( ::Distance( lpos.X, lpos.Y, lpos.Z, spos.X, spos.Y, spos.Z ) < 15000 )
									{
										wpts = GUI.WorldPosToScreen( spos + z1 );
										
										lbl.Position = VectorScreen( wpts.X, wpts.Y );
										
										local ChatBubble_PosToScreen = GUI.WorldPosToScreen( spos );
										if( ChatBubble_PosToScreen.Z < 1 )
										{
											lbl.Alpha = 255;
											lbl.Text = World.FindPlayer( index ).Name;
										}
										else
										{
											lbl.Alpha = 0;
										}
									}
											
									if( sender == lplayer )
									{
										lbl.Alpha = 0;
									}
								}
								else value.Element.Text = "";
							}
							else value.Element.Text = "";
						}
						else value.Element.Text = "";
					}
					else value.Element.Text = "";
				}
				else value.Element.Text = "";
			}
		}
	}

	function onServerData( type, str )
	{
		switch( type )
		{
			case 2500:
			this.IsSpec = true;
			break;

			case 2501:
			this.IsSpec = false;
			break;

			case 2502:
			local sp = split( str, ":" );

			this.UpdatePlayerColor( sp[0].tointeger(), sp[1].tointeger(), sp[2].tointeger(), sp[3].tointeger(), sp[4] );
			break;

			case 2503:
			this.Team = str;
			break;

			case 2504:
			switch( str )
			{
				case "true": return this.TeamESP = true;
				case "false": return this.TeamESP = false;
			}
			break;
		}
	}
}